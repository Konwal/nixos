{ config, pkgs, ... }:

{
  imports =
    [ 
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.configurationLimit = 3;
  
  boot.supportedFilesystems = [ "ntfs" ];

  boot.kernelPackages = pkgs.linuxKernel.packages.linux_zen;
  
  hardware.bluetooth.enable = true;

  networking.hostName = "vivobook";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Warsaw";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "pl_PL.UTF-8";
    LC_IDENTIFICATION = "pl_PL.UTF-8";
    LC_MEASUREMENT = "pl_PL.UTF-8";
    LC_MONETARY = "pl_PL.UTF-8";
    LC_NAME = "pl_PL.UTF-8";
    LC_NUMERIC = "pl_PL.UTF-8";
    LC_PAPER = "pl_PL.UTF-8";
    LC_TELEPHONE = "pl_PL.UTF-8";
    LC_TIME = "pl_PL.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "pl";
    xkbVariant = "";
  };

  # Configure console keymap
  console.keyMap = "pl2";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Fonts
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
  ];

  # Auto garbage collector
  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-olderthan 7d";
    };
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.konwal = {
    isNormalUser = true;
    description = "Konwal";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.fish;
    packages = with pkgs; [
    ];
  };
  
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [
    # CLI Tools 
    wget
    git
    neovim
    starship
    exa
    htop
    lolcat
    neofetch
    killall
    unzip
    unrar
    ffmpeg
    android-tools
    pmbootstrap
    # Internet
    firefox
    spotify
    lynx
    # Multimedia
    mpv
    nextcloud-client
    jellyfin-media-player
    libreoffice-qt
    # Studio
    obs-studio
    obsidian
    gimp
    lmms
    audacity
    blender
    pixelorama
    godot
    vscodium
    hugo
    # KDE Apps
    libsForQt5.alligator
    libsForQt5.kdenlive
    libsForQt5.filelight
    libsForQt5.kolourpaint
    libsForQt5.ktorrent
    libsForQt5.krdc
    libsForQt5.kdevelop
    libsForQt5.tokodon
    libsForQt5.neochat
    libsForQt5.kasts
    libsForQt5.kate
    krita
    arianna
    # Games
    steam
    lutris
    airshipper
    protonup-qt
    prismlauncher
    cemu
    citra-nightly
    yuzu-mainline
    dolphin-emu
    pcsx2
    rpcs3
    (retroarch.override {
      cores = with libretro; [
        parallel-n64
        snes9x
	citra
	mgba
	dolphin
	desmume
      ];
    })
    # Other
    cmake
    gnumake
    glaxnimate
    mediainfo
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  services = {
    flatpak.enable = true;
  };
  # List programs that you want to enable:
  programs = {
    kdeconnect.enable = true;
    gamemode.enable = true;
    fish.enable = true;
  };
  # Enable the OpenSSH daemon.
    services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
    networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
